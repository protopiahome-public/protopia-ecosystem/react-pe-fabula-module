import React from "react"
import { IFabulaHero, IPEStory } from "@/modules/pe-fabula-module/data/interfaces"  
import { IStorytellingStore, useStorytellingStore } from "src/modules/pe-fabula-module/data/store"
import { intToTime } from "src/libs/utilities"
import RoundSlider from "./RoundSlider"

/* Protopia Ecosystem component */
export interface IFabulaTimerProps {

}
const FabulaTimer = (props: IFabulaTimerProps): JSX.Element => {
    const timer : number = useStorytellingStore((state: IStorytellingStore) => state.timer)
    const PeStory : IPEStory = useStorytellingStore((state: IStorytellingStore) => state.current)
    const hero : IFabulaHero   = useStorytellingStore((state: IStorytellingStore) => state.hero)
    const value = Math.floor( timer / PeStory.duration * 10000 ) / 100 
    return <div 
        className='pe-fabula-timer-container'
           
    >
        <RoundSlider value={value }  />
        <div 
            className="pe-fabula-timer"
            style={{
                backgroundImage: `url(${ hero.avatar })`
            }}
        >
            <div className="pe-fabula-timer-tablo">
                { intToTime(timer) }
            </div>
        </div> 
    </div>
}
export default FabulaTimer


interface IValue {
    index: number
    value: number
}
function Tick({ index, value }: IValue) {
    const { x1, y1, x2, y2, color } = deriveData(index, value);
    return /*#__PURE__*/(
        React.createElement("line", {
            x1: x1,
            y1: y1,
            x2: x2,
            y2: y2,
            stroke: color,
            strokeWidth: "3",
            strokeLinecap: "round"
        }));


}

function deriveData(index: number, value: number) {
    const r1 = 130;

    const r2 = 150;
    const r3 = 140;
    const delta = Math.PI / 40;
    const angle = delta * index - Math.PI;

    const ss = Math.sin(angle);
    const cc = Math.cos(angle);

    const rs = index % 5 === 0 ? r1 : r3;

    const x1 = rs * cc;
    const y1 = rs * ss;
    const x2 = r2 * cc;
    const y2 = r2 * ss;

    const color = Math.ceil(value * (41 / 100)) > index ? "#424E82" : "#E8EBF9";
    return { x1, y1, x2, y2, color };
}