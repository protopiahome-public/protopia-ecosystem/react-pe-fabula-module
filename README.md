# React PE Fabula Module
 
<p align="center"> 
    <img src="https://gitlab.com/uploads/-/system/project/avatar/53088378/pe_logo_20.jpg" width="100" alt="Fabula Logo" /> 
</p> 

![React](https://img.shields.io/badge/-React-black?style=flat-square&logo=react)
![HTML5](https://img.shields.io/badge/-HTML5-E34F26?style=flat-square&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/-CSS3-1572B6?style=flat-square&logo=css3)
![Bootstrap](https://img.shields.io/badge/-Bootstrap-563D7C?style=flat-square&logo=bootstrap)
![TypeScript](https://img.shields.io/badge/-TypeScript-007ACC?style=flat-square&logo=typescript)
![GraphQL](https://img.shields.io/badge/-GraphQL-E10098?style=flat-square&logo=graphql)
![Apollo GraphQL](https://img.shields.io/badge/-Apollo%20GraphQL-311C87?style=flat-square&logo=apollo-graphql)

Движок для создания и проигрывания многопользовательских визуальных новелл и DND-фонов для сюжетно-ролевых игр живого действия.
